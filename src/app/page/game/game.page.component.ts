import { Component } from "@angular/core";
import { GameService } from "../../services/game.service";
import { ActivatedRoute, Params } from "@angular/router";
import { Game } from "../../models/game";

@Component({
    templateUrl: 'game.page.component.html'
})
export class GamePageComponent {
    game: Game;
    autoLoad: boolean = false;

    constructor( private gameService: GameService, private route: ActivatedRoute){
        this.route.params.subscribe((data: Params) => {
            this.onRouteParamsChanged(data);
        });
    }
    private onRouteParamsChanged(params: Params) {
        let id = params['id'];
        this.gameService.findById(id).subscribe(
            (data: Game) => {
                this.game = data;
            },
            (error: any) => {
                alert(error.message);
            }
            
        );
    }

    onSwitchAutoClick() {
        this.autoLoad = !this.autoLoad;
        if(this.autoLoad) {
            this.gameService.save(this.game).subscribe((data: any) => {
                this.onNextButtonClick();
            });
        }
    }

    onNextButtonClick() {
        this.gameService.next(this.game.id).subscribe((data: any)=>{
            this.game = data;
            if(this.autoLoad) {
                this.onNextButtonClick();
            }
        });
    }
}