import { Component, AfterViewInit } from "@angular/core";
import { Game } from "../../models/game";
import { GameService } from "../../services/game.service";
import { Router } from "@angular/router";

declare var $;

@Component({
    templateUrl: 'home.page.component.html'
})
export class HomePageComponent implements AfterViewInit {

    game: Game;

    constructor(private gameService: GameService, private router: Router){}

    ngAfterViewInit(): void {
        $(document).ready(function(){
            $('select').formSelect();
          });
    }

    onStartButtonClick(){
        this.gameService.save(this.game).subscribe(
            (data: Game) => {
                this.router.navigate(['/game/'+ data.id])
            },
            (error: any) => {
                alert(error.message);
            }
        )
    }

    sizeChanged(option: string) {
        let game = new Game();
        game.activeCells = [];
        switch (option) {
            case '1':
                game.width = 100;
                game.height = 80;
                break;
            case '2':
                game.width = 80;
                game.height = 40;
                break;
            case '3':
                game.width = 100;
                game.height = 100;
                break;
        }
        this.game = game;
    }
}