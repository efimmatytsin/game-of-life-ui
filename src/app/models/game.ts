import { Cell } from "./cell";

export class Game {
    id: string;
    width: number;
    height: number;
    activeCells: Cell[];
}