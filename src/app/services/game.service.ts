import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Game } from "../models/game";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from "@angular/http";
import { Cell } from "../models/cell";

@Injectable()
export class GameService {

    BACKEND_URL = 'http://localhost:8080/';

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient) {}

    create(height: number, width: number): Game {
        let result = new Game();
        result.width = width;
        result.height = height;
        result.activeCells = [];
        return result;
    }

    save(game: Game): Observable<Object> {
        return this.http.post(this.BACKEND_URL+'api/game/', JSON.stringify(game), this.httpOptions );
    }

    findById(id: string): Observable<Object> {
        return this.http.get(this.BACKEND_URL+'api/game/' + id, this.httpOptions );
    }

    next(id: string): Observable<Object> {
        return this.http.get(this.BACKEND_URL+'api/game/' + id + '/next', this.httpOptions );
    }
}