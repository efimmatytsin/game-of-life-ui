import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CookieXSRFStrategy, HttpModule, XSRFStrategy} from '@angular/http';


import { AppComponent } from './app.component';
import { BoardComponent } from './components/board/board.component';
import { GameService } from './services/game.service';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './page/home/home.page.component';
import { GamePageComponent } from './page/game/game.page.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    HomePageComponent,
    GamePageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomePageComponent
      },
      {
        path: 'game/:id',
        component: GamePageComponent
      }
    ])
  ],
  providers: [
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
